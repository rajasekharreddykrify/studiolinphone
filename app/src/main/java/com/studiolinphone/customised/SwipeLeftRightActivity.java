package com.studiolinphone.customised;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.studiolinphone.R;

import java.util.ArrayList;

public class SwipeLeftRightActivity extends AppCompatActivity {

    RecyclerView swipe_left_right_rv;
    ArrayList<String> data;

    LinearLayoutManager mLayoutManager;
    SwipeLeftRightAdapter adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_left_right);

        swipe_left_right_rv = (RecyclerView) findViewById(R.id.swipe_left_right_rv);


        data = new ArrayList<String>();

        for (int i = 0; i < 100; i++) {

            data.add("Item Number is +"+i);
        }

        mLayoutManager = new LinearLayoutManager(SwipeLeftRightActivity.this);

        swipe_left_right_rv.setLayoutManager(mLayoutManager);

        adapter = new SwipeLeftRightAdapter(SwipeLeftRightActivity.this,data);

        swipe_left_right_rv.setAdapter(adapter);



    }
}
