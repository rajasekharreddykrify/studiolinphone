package com.studiolinphone.setup;
/*
GenericLoginFragment.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.studiolinphone.LinphoneManager;
import com.studiolinphone.LinphonePreferences;
import com.studiolinphone.R;

import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.PayloadType;

/**
 * @author Sylvain Berfini
 */
public class GenericLoginFragment extends Fragment implements OnClickListener {
	private EditText login, password, domain;
	private ImageView apply;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.setup_generic_login, container, false);
		
		login = (EditText) view.findViewById(R.id.setup_username);
		password = (EditText) view.findViewById(R.id.setup_password);
		domain = (EditText) view.findViewById(R.id.setup_domain);
		apply = (ImageView) view.findViewById(R.id.setup_apply);
		apply.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.setup_apply) {
			if (login.getText() == null || login.length() == 0 || password.getText() == null || password.length() == 0 || domain.getText() == null || domain.length() == 0) {
				Toast.makeText(getActivity(), getString(R.string.first_launch_no_login_password), Toast.LENGTH_LONG).show();
				return;
			}
			
			setDefaults(getActivity());
			
			SetupActivity.instance().genericLogIn(login.getText().toString(), password.getText().toString(), domain.getText().toString());
		}
	}
	
	
	public static void setDefaults(Activity activity) {

		// Default settings
		LinphonePreferences mPrefs = LinphonePreferences.instance();

		mPrefs.setAutoStart(false);
		// mPrefs.setAccountOutboundProxyEnabled(0, true);

		// Audio states

		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();

		// reopen to update download ptime lc.setDownloadPtime(ptime);

		int i = 0;
		for (final PayloadType pt : lc.getAudioCodecs()) {

			// if(TextUtils.equals(pt.getMime().toString(),
			// getResources().getString(R.string.AudioCodec1))){

			try {

				Log.d("audiocodec : " + i++, pt.getMime().toString());
				Log.d("audiocodecfreq : " + i++, pt.getRate() + " Hz");
				Log.d("----------------", "------------------");

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec1))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");
				} /*else {
					lc.enablePayloadType(pt, false);
				}*/

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec2))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");

				} /*else {
					lc.enablePayloadType(pt, false);
				}*/

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec3))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");

				}/* else {
					lc.enablePayloadType(pt, false);
				}*/

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec4))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");

				} /*else {
					lc.enablePayloadType(pt, false);
				}*/

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec5))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");

				} /*else {
					lc.enablePayloadType(pt, false);
				}*/

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.AudioCodec6))) {

					lc.enablePayloadType(pt, true);
					Log.i("audiocodec enabled: ", pt.getMime().toString());
					Log.i("audiocodecfreq ","audiocodecfreq enabled: "+ pt.getRate() + " Hz");

				} /*else {
					lc.enablePayloadType(pt, false);
				}*/

			} catch (LinphoneCoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				Log.e("Audio codec", "codec not enabled");
			}
			// }
			// lc.isPayloadTypeEnabled(pt);

		}
		//mPrefs.EchoCancellationEnable();

		// Video states

		i = 0;
		for (final PayloadType pt : lc.getVideoCodecs()) {

			try {

				Log.w("videocodec : " + i++, pt.getMime().toString());
				Log.w("videocodecfreq : " + i++, pt.getRate() + " Hz");
				Log.w("----------------", "------------------");

				if (TextUtils.equals(pt.getMime().toString(), activity
						.getResources().getString(R.string.VideoCodec1))) {
					lc.enablePayloadType(pt, true);

				} else {
					lc.enablePayloadType(pt, false);

				}

			} catch (LinphoneCoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				Log.e("Audio codec", "codec not enabled");
			}

		}

//		//CustomPreferences CP1 = new CustomPreferences(activity);
//		// Toast.makeText(activity,
//		// "Welcome Stun value is: "+CP1.getStun().toString(),
//		// Toast.LENGTH_LONG).show();
//		// Log.i("Welcome Stun value","Welcome Stun value is: "+CP1.getStun().toString());
//		lc.setStunServer(CP1.getStun());
//
//		lc.setFirewallPolicy(FirewallPolicy.UseStun);

		// Network states

		mPrefs.useRandomPort(true);
		mPrefs.setPushNotificationEnabled(true);

		// Advanced States

		mPrefs.setBackgroundModeEnabled(true);
		mPrefs.setAutoStart(true);

	}
}
