package com.studiolinphone.customised;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.studiolinphone.R;
import com.studiolinphone.swipe.SwipeLayout;
import com.studiolinphone.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;


public class SwipeLeftRightAdapter extends RecyclerSwipeAdapter<SwipeLeftRightAdapter.SimpleViewHolder> {


    private Context mContext;
    private ArrayList<String> itemsData;


    public SwipeLeftRightAdapter(Context context, ArrayList<String> itemsData) {
        this.mContext = context;
        this.itemsData = itemsData;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cutomised_item_swipe_left_right, parent, false);
        return new SimpleViewHolder(view);
    }



    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {

        viewHolder.item_name.setText( "" + itemsData.get(position));


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
          viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));

        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper1));



        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
                Log.i("onClose", "onClose");
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
                Log.i("onUpdate", "onUpdate");


            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                Log.i("onStartOpen", "onStartOpen");


                // mItemManger.closeAllItems();

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
                Log.i("onOpen", "onOpen");


            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                Log.i("onStartClose", "onStartClose");

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
                Log.i("onHandRelease", "onHandRelease");

            }
        });



        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, " onClick : " + item.getName() + " \n" + item.getItem_name()
//                        , Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



//              frag.cancelOffer(position);
//                frag.offerCancelApiCall(item.getOffer_id(),position);



//                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
//                itemsData.remove(position);
//                notifyItemRemoved(position);
//                notifyItemRangeChanged(position, itemsData.size());
                mItemManger.closeAllItems();

//                    Toast.makeText(mContext, " Cancel : " + item.getName() + " \n" + item.getItem_name()
//                            , Toast.LENGTH_SHORT).show();

            }
        });

        // mItemManger is member in RecyclerSwipeAdapter Class
//        mItemManger.bindView(viewHolder.itemView, position);

        mItemManger.bind(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        public TextView item_name;
        public LinearLayout wholelayout;
        TextView tvDelete;
        TextView tvEdit;
        TextView cancel;


        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            item_name=(TextView)itemView.findViewById(R.id.name_tv);

            wholelayout=(LinearLayout)itemView.findViewById(R.id.wholelayout);
            tvDelete = (TextView) itemView.findViewById(R.id.txt_delete);
            tvEdit = (TextView) itemView.findViewById(R.id.txt_undo);

            cancel = (TextView) itemView.findViewById(R.id.buyer_offer_submit_item_cancel_tv);


        }
    }
}
