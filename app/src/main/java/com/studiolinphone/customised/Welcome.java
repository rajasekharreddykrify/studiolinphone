package com.studiolinphone.customised;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.studiolinphone.R;
import com.studiolinphone.setup.GenericLoginFragment;
import com.studiolinphone.setup.SetupActivity;


public class Welcome extends Activity {

	Button login, call, clear;
	EditText number;
	TextView status;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.welcome);

		login = (Button) findViewById(R.id.login_btn);
		call = (Button) findViewById(R.id.call_btn);
		clear = (Button) findViewById(R.id.clear_btn);

		number = (EditText) findViewById(R.id.call_number_et);

		status = (TextView) findViewById(R.id.status_tv);

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (SetupActivity.isInstanciated()) {
					GenericLoginFragment.setDefaults(Welcome.this);
					SetupActivity.instance().genericLogIn(
							getResources().getString(R.string.custom_username),
							getResources().getString(R.string.custom_password),
							getResources().getString(R.string.custom_domain));

				}else{
					Toast.makeText(Welcome.this, "SetupActivity not Instanciated yet",
							Toast.LENGTH_SHORT).show();
				}

			}
		});
		call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (number.getText().toString().trim().length() == 0) {
					Toast.makeText(Welcome.this, "Please enter number...",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(Welcome.this, "Calling...",
							Toast.LENGTH_SHORT).show();
				}

			}
		});
		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

	}

}
