package com.studiolinphone.customised.weather;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.studiolinphone.R;
import com.studiolinphone.StudioLinphone;

import org.json.JSONException;


/*
 * Copyright (C) 2013 Surviving with Android (http://www.survivingwithandroid.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class WeatherActivity extends Activity {

	private static final String TAG = "WeatherActivity";
	
	private TextView cityText;
	private TextView condDescr;
	private TextView temp;
	private TextView press;
	private TextView windSpeed;
	private TextView windDeg;
	
	private TextView hum;
	private ImageView imgView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weather);
		String city = "London,UK";
		
		cityText = (TextView) findViewById(R.id.cityText);
		condDescr = (TextView) findViewById(R.id.condDescr);
		temp = (TextView) findViewById(R.id.temp);
		hum = (TextView) findViewById(R.id.hum);
		press = (TextView) findViewById(R.id.press);
		windSpeed = (TextView) findViewById(R.id.windSpeed);
		windDeg = (TextView) findViewById(R.id.windDeg);
		imgView = (ImageView) findViewById(R.id.condIcon);
		
//		JSONWeatherTask task = new JSONWeatherTask();
//		task.execute(new String[]{city});
	}


	public static void weatherUpdates(String lat,String lng){

		Log.d(TAG, "weatherUpdates() called with: " + "lat = [" + lat + "], lng = [" + lng + "]");
		//			location = "lat=16.996927&lon=82.223012";
		String location = "lat="+lat+"&lon="+lng;
		Log.i(TAG, "weatherUpdates: location is : "+location);
//		if(!location.equals("0.0,0.0")){
			JSONWeatherTask task = new JSONWeatherTask();
			task.execute(new String[]{location});
//		}else{
//
//			Intent in = new Intent(StudioLinphone.getAppContext(), PlayLocation.class);
//			StudioLinphone.getAppContext().startService(in);
//			Log.i(TAG, "weatherUpdates: Location not found");
//		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_studio_linphone, menu);
		return true;
	}

	
	private static class JSONWeatherTask extends AsyncTask<String, Void, Weather> {
		
		@Override
		protected Weather doInBackground(String... params) {
			Weather weather = new Weather();
			String data = ( (new WeatherHttpClient()).getWeatherData(params[0]));

			try {
				
				Log.e("data","data is : "+data);
				weather = JSONWeatherParser.getWeather(data);
				
				// Let's retrieve the icon
				//weather.iconData = ( (new WeatherHttpClient()).getImage(weather.currentCondition.getIcon()));
				
			} catch (JSONException e) {				
				e.printStackTrace();
			}
			return weather;
		
	}
		
		
		
		
	@Override
		protected void onPostExecute(Weather weather) {			
			super.onPostExecute(weather);
			
//			if (weather.iconData != null && weather.iconData.length > 0) {
//				Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length);
////				imgView.setImageBitmap(img);
//			}
			
//			cityText.setText(weather.location.getCity() + "," + weather.location.getCountry());
//			condDescr.setText(weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")");
//			temp.setText("" + Math.round((weather.temperature.getTemp() - 273.15)) + "�C");
//			hum.setText("" + weather.currentCondition.getHumidity() + "%");
//			press.setText("" + weather.currentCondition.getPressure() + " hPa");
//			windSpeed.setText("" + weather.wind.getSpeed() + " mps");
//			windDeg.setText("" + weather.wind.getDeg() + "�");

		Toast.makeText(StudioLinphone.getAppContext(),"city : "+weather.location.getCity() + "," + weather.location.getCountry()+"\n"+
				"Condition : "+weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")"+"\n"+
				"temperature : "+"" + Math.round((weather.temperature.getTemp() - 273.15)) + "�C"+"\n"+
				"Humidity : "+"" + weather.currentCondition.getPressure() + " hPa"+"\n"+
				"wind : "+"" + weather.wind.getDeg() + "�",Toast.LENGTH_SHORT).show();




		try {
			Log.i(TAG, "city : "+weather.location.getCity() + "," + weather.location.getCountry());
			Log.i(TAG, "Condition : "+weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")");
			Log.i(TAG, "temperature : "+"" + Math.round((weather.temperature.getTemp() - 273.15)) + "�C");
			Log.i(TAG, "Humidity : "+"" + weather.currentCondition.getPressure() + " hPa");
			Log.i(TAG, "wind : "+"" + weather.wind.getDeg() + "�");
		} catch (Exception e) {
			Log.e(TAG, "onPostExecute: Exception");
			e.printStackTrace();
		}

	}






	
  }
}
