package com.studiolinphone.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.studiolinphone.StudioLinphone;
import com.studiolinphone.customised.weather.WeatherActivity;


/**
 * Created by raj on 9/2/16.
 */
public class WeatherService extends Service {

    private static final String TAG = "InfinyService";

    private static final long WEATHER_UPDATE_TIME = 10*60*1000;

    // flag that should be set true if handler should stop
    boolean mStopHandler = false;
    Handler mHandler = new Handler();

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // do your stuff - don't create a new runnable here!
            if (!mStopHandler) {

                if(PlayLocation.playlat.equals("0.0")||PlayLocation.playlog.equals("0.0")){
                    Intent in = new Intent(StudioLinphone.getAppContext(), PlayLocation.class);
                    StudioLinphone.getAppContext().startService(in);
                    Log.i(TAG, "weatherUpdates: Location not found");
                }else{
                    WeatherActivity.weatherUpdates(PlayLocation.playlat , PlayLocation.playlog);
                    Log.i(TAG, "Update Weather Details");


                }
                mHandler.postDelayed(this, WEATHER_UPDATE_TIME);

            }
        }
    };



    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "onCreate: ");

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        Log.i(TAG, "onBind: ");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);

        Log.i(TAG, "onStartCommand: Weather Service Started");

        // start it with:
        mHandler.post(runnable);

//        Toast.makeText(getApplicationContext(),"Weather Service Started",Toast.LENGTH_SHORT).show();
        return  START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy: Weather Service Destroyed");

        mHandler.removeCallbacks(runnable);
//        Toast.makeText(getApplicationContext(),"Weather Service Destroyed",Toast.LENGTH_SHORT).show();

    }




}
