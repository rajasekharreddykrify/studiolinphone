package com.studiolinphone.compatibility;


public interface CompatibilityScaleGestureListener {
	public boolean onScale(CompatibilityScaleGestureDetector detector);
}