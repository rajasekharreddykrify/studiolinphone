package com.studiolinphone.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.pathsense.android.sdk.location.PathsenseDetectedActivities;
import com.pathsense.android.sdk.location.PathsenseDetectedActivity;
import com.pathsense.android.sdk.location.PathsenseDeviceHolding;
import com.pathsense.android.sdk.location.PathsenseLocationProviderApi;

import java.util.List;

/**
 * Created by raj on 10/2/16.
 */
public class PathSenseService extends Service {


    //
    static final String TAG = PathSenseService.class.getName();
    // Messages
    static final int MESSAGE_ON_ACTIVITY_CHANGE = 1;
    static final int MESSAGE_ON_ACTIVITY_UPDATE = 2;
    static final int MESSAGE_ON_DEVICE_HOLDING = 3;
    //
    InternalActivityChangeReceiver mActivityChangeReceiver;
    InternalActivityUpdateReceiver mActivityUpdateReceiver;
    InternalDeviceHoldingReceiver mDeviceHoldingReceiver;
    InternalHandler mHandler;
    PathsenseLocationProviderApi mApi;



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "onCreate: ");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "onStartCommand: PathSense Service Started");
        Toast.makeText(getApplicationContext(), "PathSense Service Started", Toast.LENGTH_SHORT).show();

        mHandler = new InternalHandler(this);
        // receivers
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        mActivityChangeReceiver = new InternalActivityChangeReceiver(this);
        localBroadcastManager.registerReceiver(mActivityChangeReceiver, new IntentFilter("activityChange"));
        mActivityUpdateReceiver = new InternalActivityUpdateReceiver(this);
        localBroadcastManager.registerReceiver(mActivityUpdateReceiver, new IntentFilter("activityUpdate"));
        mDeviceHoldingReceiver = new InternalDeviceHoldingReceiver(this);
        localBroadcastManager.registerReceiver(mDeviceHoldingReceiver, new IntentFilter("deviceHolding"));
        // location api
        mApi = PathsenseLocationProviderApi.getInstance(this);



        return super.onStartCommand(intent, flags, startId);



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "PathSense Service Destroyed", Toast.LENGTH_SHORT).show();

        Log.i(TAG, "onDestroy: PathSense Service Destroyed");

        final PathsenseLocationProviderApi api = mApi;
        //
        if (api != null)
        {
            api.removeActivityChanges();
            api.removeActivityUpdates();
            api.removeDeviceHolding();
        }
    }













    static class InternalActivityChangeReceiver extends BroadcastReceiver
    {
        PathSenseService mActivity;
        //
        InternalActivityChangeReceiver(PathSenseService activity)
        {
            mActivity = activity;
        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final PathSenseService activity = mActivity;
            final InternalHandler handler = activity != null ? activity.mHandler : null;
            //
            if (activity != null && handler != null)
            {
                PathsenseDetectedActivities detectedActivities = (PathsenseDetectedActivities) intent.getSerializableExtra("detectedActivities");
                Message msg = Message.obtain();
                msg.what = MESSAGE_ON_ACTIVITY_CHANGE;
                msg.obj = detectedActivities;
                handler.sendMessage(msg);
            }
        }
    }
    static class InternalActivityUpdateReceiver extends BroadcastReceiver
    {
        PathSenseService mActivity;
        //
        InternalActivityUpdateReceiver(PathSenseService activity)
        {
            mActivity = activity;
        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final PathSenseService activity = mActivity;
            final InternalHandler handler = activity != null ? activity.mHandler : null;
            //
            if (activity != null && handler != null)
            {
                PathsenseDetectedActivities detectedActivities = (PathsenseDetectedActivities) intent.getSerializableExtra("detectedActivities");
                Message msg = Message.obtain();
                msg.what = MESSAGE_ON_ACTIVITY_UPDATE;
                msg.obj = detectedActivities;
                handler.sendMessage(msg);
            }
        }
    }
    static class InternalDeviceHoldingReceiver extends BroadcastReceiver
    {
        PathSenseService mActivity;
        //
        InternalDeviceHoldingReceiver(PathSenseService activity)
        {
            mActivity = activity;
        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final PathSenseService activity = mActivity;
            final InternalHandler handler = activity != null ? activity.mHandler : null;
            //
            if (activity != null && handler != null)
            {
                PathsenseDeviceHolding deviceHolding = (PathsenseDeviceHolding) intent.getSerializableExtra("deviceHolding");
                Message msg = Message.obtain();
                msg.what = MESSAGE_ON_DEVICE_HOLDING;
                msg.obj = deviceHolding;
                handler.sendMessage(msg);
            }
        }
    }
    static class InternalHandler extends Handler
    {
        PathSenseService mActivity;
        //
        InternalHandler(PathSenseService activity)
        {
            mActivity = activity;
        }
        @Override
        public void handleMessage(Message msg)
        {
            final PathSenseService activity = mActivity;
//            final TextView textDetectedActivity0 = activity != null ? activity.mTextDetectedActivity0 : null;
//            final TextView textDetectedActivity1 = activity != null ? activity.mTextDetectedActivity1 : null;
//            final TextView textDeviceHolding = activity != null ? activity.mTextDeviceHolding : null;
            final PathsenseLocationProviderApi api = activity != null ? activity.mApi : null;
            //
            if (activity != null && api != null)
            {
                switch (msg.what)
                {
                    case MESSAGE_ON_ACTIVITY_CHANGE:
                    {
                        PathsenseDetectedActivities detectedActivities = (PathsenseDetectedActivities) msg.obj;
                        PathsenseDetectedActivity mostProbableActivity = detectedActivities.getMostProbableActivity();
                        if (mostProbableActivity != null)
                        {
                            StringBuilder detectedActivityString = new StringBuilder(mostProbableActivity.getDetectedActivity().name());
//							if (mostProbableActivity.isStationary())
//							{
//								detectedActivityString.append(" STATIONARY");
//							}
//                            textDetectedActivity1.setText(detectedActivityString.toString());

                            Toast.makeText(activity,detectedActivityString.toString(),Toast.LENGTH_SHORT).show();
                            Log.i(TAG, "MESSAGE_ON_ACTIVITY_CHANGE: " + detectedActivityString.toString());
                        } else
                        {
                            Log.i(TAG, "MESSAGE_ON_ACTIVITY_CHANGE: "+ "Empty");

//                            textDetectedActivity1.setText("");
                        }
                        break;
                    }
                    case MESSAGE_ON_ACTIVITY_UPDATE:
                    {
                        PathsenseDetectedActivities detectedActivities = (PathsenseDetectedActivities) msg.obj;
                        PathsenseDetectedActivity mostProbableActivity = detectedActivities.getMostProbableActivity();
                        if (mostProbableActivity != null)
                        {
                            List<PathsenseDetectedActivity> detectedActivityList = detectedActivities.getDetectedActivities();
                            int numDetectedActivityList = detectedActivityList != null ? detectedActivityList.size() : 0;
                            if (numDetectedActivityList > 0)
                            {
                                StringBuilder detectedActivityString = new StringBuilder();
                                for (int i = 0; i < numDetectedActivityList; i++)
                                {
                                    PathsenseDetectedActivity detectedActivity = detectedActivityList.get(i);
                                    if (i > 0)
                                    {
                                        detectedActivityString.append("<br />");
                                    }
                                    detectedActivityString.append(detectedActivity.getDetectedActivity().name() + " " + detectedActivity.getConfidence());
                                }
                                Toast.makeText(activity,detectedActivityString.toString(),Toast.LENGTH_SHORT).show();

                                Log.i(TAG, "MESSAGE_ON_ACTIVITY_UPDATE: "+detectedActivityString.toString());
//                                textDetectedActivity0.setText(Html.fromHtml(detectedActivityString.toString()));
                            }
                        } else
                        {
                            Log.i(TAG, "MESSAGE_ON_ACTIVITY_UPDATE: "+"Empty");

//                            textDetectedActivity0.setText("");
                        }
                        break;
                    }
                    case MESSAGE_ON_DEVICE_HOLDING:
                    {
                        PathsenseDeviceHolding deviceHolding = (PathsenseDeviceHolding) msg.obj;
                        if (deviceHolding != null)
                        {
                            String s = deviceHolding.isHolding() ? "Holding" : "Not Holding";
                            Log.i(TAG, "MESSAGE_ON_DEVICE_HOLDING: "+s);
                            Toast.makeText(activity,s,Toast.LENGTH_SHORT).show();

//                            textDeviceHolding.setText(deviceHolding.isHolding() ? "Holding" : "Not Holding");
                        } else
                        {
                            Log.i(TAG, "MESSAGE_ON_DEVICE_HOLDING: "+"Empty");

//                            textDeviceHolding.setText("");
                        }
                        break;
                    }
                }
            }
        }
    }


}
