package com.studiolinphone.customised;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AlphabetIndexer;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.studiolinphone.Contact;
import com.studiolinphone.ContactsManager;
import com.studiolinphone.LinphoneActivity;
import com.studiolinphone.LinphoneManager;
import com.studiolinphone.LinphoneUtils;
import com.studiolinphone.R;
import com.studiolinphone.compatibility.Compatibility;
import com.studiolinphone.customised.swipeadapters.ListViewAdapter;
import com.studiolinphone.customised.swipeadapters.SwipeToDismissTouchListener;

import org.linphone.core.LinphoneFriend;
import org.linphone.core.PresenceActivityType;

import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

public class PhoneContacts extends AppCompatActivity {

    ListView phoneContactsList;
    private Cursor searchCursor;

    private LayoutInflater mInflater;
    private AlphabetIndexer indexer;

    ContactsListAdapter contactsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_contacts);

        phoneContactsList = (ListView) findViewById(R.id.phoneContactsList);

       init(phoneContactsList);
    }



    private void init(ListView listView) {
//        final MyBaseAdapter adapter = new MyBaseAdapter();
//        listView.setAdapter(adapter);
        searchCursor = Compatibility.getContactsCursor(getContentResolver(), "", ContactsManager.getInstance().getContactsId());

        indexer = new AlphabetIndexer(searchCursor, Compatibility.getCursorDisplayNameColumnIndex(searchCursor), " ABCDEFGHIJKLMNOPQRSTUVWXYZ");

        contactsListAdapter = new ContactsListAdapter(null, searchCursor);


        phoneContactsList.setAdapter(contactsListAdapter);

        final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                new SwipeToDismissTouchListener<ListViewAdapter>(
                        new ListViewAdapter(listView),
                        new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListViewAdapter view, int position) {
                                contactsListAdapter.remove(position);
                            }
                        });
        listView.setOnTouchListener(touchListener);
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes.
        listView.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss();
                } else {
                    Toast.makeText(PhoneContacts.this, "Position " + position, LENGTH_SHORT).show();
                }
            }
        });
    }


    class ContactsListAdapter extends BaseAdapter implements SectionIndexer {
        private int margin;
        private Bitmap bitmapUnknown;
        private List<Contact> contacts;
        private Cursor cursor;

        ContactsListAdapter(List<Contact> contactsList, Cursor c) {
            contacts = contactsList;
            cursor = c;

            margin = LinphoneUtils.pixelsToDpi(LinphoneActivity.instance().getResources(), 10);
            bitmapUnknown = BitmapFactory.decodeResource(LinphoneActivity.instance().getResources(), R.drawable.unknown_small);
        }

        public int getCount() {
            return cursor.getCount();
        }

        public Object getItem(int position) {
            if (contacts == null || position >= contacts.size()) {
                return Compatibility.getContact( getContentResolver(), cursor, position);
            } else {
                return contacts.get(position);
            }
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {


            View view = null;
            Contact contact = null;



            do {
                contact = (Contact) getItem(position);
            } while (contact == null);

            if (convertView != null) {
                view = convertView;
            } else {

                view = LayoutInflater.
                        from(parent.getContext()).
                        inflate(R.layout.contact_cell, parent, false);
//                view = mInflater.inflate(R.layout.contact_cell, parent, false);
            }

            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(contact.getName());

            TextView separator = (TextView) view.findViewById(R.id.separator);
            LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout);
            if (getPositionForSection(getSectionForPosition(position)) != position) {
                separator.setVisibility(View.GONE);
                layout.setPadding(0, margin, 0, margin);
            } else {
                separator.setVisibility(View.VISIBLE);
                separator.setText(String.valueOf(contact.getName().charAt(0)));
                layout.setPadding(0, 0, 0, margin);
            }

            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            if (contact.getPhoto() != null) {
                icon.setImageBitmap(contact.getPhoto());
            } else if (contact.getPhotoUri() != null) {
                icon.setImageURI(contact.getPhotoUri());
            } else {
                icon.setImageBitmap(bitmapUnknown);
            }

            ImageView friendStatus = (ImageView) view.findViewById(R.id.friendStatus);
            LinphoneFriend[] friends = LinphoneManager.getLc().getFriendList();
            if (!ContactsManager.getInstance().isContactPresenceDisabled() && friends != null) {
                friendStatus.setVisibility(View.VISIBLE);
                PresenceActivityType presenceActivity = friends[0].getPresenceModel().getActivity().getType();
                if (presenceActivity == PresenceActivityType.Online) {
                    friendStatus.setImageResource(R.drawable.led_connected);
                } else if (presenceActivity == PresenceActivityType.Busy) {
                    friendStatus.setImageResource(R.drawable.led_error);
                } else if (presenceActivity == PresenceActivityType.Away) {
                    friendStatus.setImageResource(R.drawable.led_inprogress);
                } else if (presenceActivity == PresenceActivityType.Offline) {
                    friendStatus.setImageResource(R.drawable.led_disconnected);
                } else {
                    friendStatus.setImageResource(R.drawable.call_quality_indicator_0);
                }
            }

            return view;
        }

        @Override
        public int getPositionForSection(int section) {
            return indexer.getPositionForSection(section);
        }

        @Override
        public int getSectionForPosition(int position) {
            return indexer.getSectionForPosition(position);
        }

        @Override
        public Object[] getSections() {
            return indexer.getSections();
        }


        public void remove(int position) {
//            contacts.remove(position);

            notifyDataSetChanged();
        }
    }
}
