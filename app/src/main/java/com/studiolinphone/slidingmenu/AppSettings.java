package com.studiolinphone.slidingmenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import com.studiolinphone.R;

public class AppSettings extends AppCompatActivity {

    ToggleButton sleepToggleButton;

    private static final String TAG = "AppSettings";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        sleepToggleButton = (ToggleButton) findViewById(R.id.appSettingsToggleButton);






    }

    /**
     * Click event of the togglebutton1 is declared in the layout xml file itself.
     * @param view
     */
    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            Log.i("info", "Button1 is on!");

//            Toast.makeText(AppSettings.this,)
//            textView1.setText("Button1 is ON");
        } else {
            Log.i("info", "Button1 is off!");
//            textView1.setText("Button1 is OFF");
        }
    }
}
